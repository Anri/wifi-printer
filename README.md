# Wi-Fi printer

## Usage

Put [`wifi.cls`](./wifi.cls) file along with your `.tex` file.

In your `.tex` file :

```latex
\documentclass{wifi}
```

Options available:

|  Option  | Meaning                | default   |
| :------: | :--------------------- | --------- |
|  `size`  | Font size              | `45`      |
| `qrcode` | QR codes height        | `7cm`     |
| `border` | Border around QR codes | No border |

Commands available:

|          Command          | Meaning                                  |
| :-----------------------: | :--------------------------------------- |
|  `\qrc{password}{ssid}`   | Insert a QR code                         |
| `\insertspaces{password}` | Insert a little space every 4 characters |

## [Examples](./examples)

See [examples](./examples) of use.

|    [`one.tex`](./examples/one.tex)    |    [`two.tex`](./examples/two.tex)    |
| :-----------------------------------: | :-----------------------------------: |
| ![](https://i.imgur.com/l1cxXGW.jpeg) | ![](https://i.imgur.com/9yAS9sD.jpeg) |
