\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{wifi}[2024/05/11 Print WiFi password]

% Options
\RequirePackage{kvoptions}
\DeclareStringOption[45]{size}    % font size
\DeclareStringOption[7cm]{qrcode} % QR codes height
\DeclareBoolOption{border}        % QR codes border
\ProcessKeyvalOptions*

\LoadClass[landscape]{article}

% Metadatas
\RequirePackage[
  pdftitle={Code Wi-Fi},
  pdfsubject={Wi-Fi},
  pdfkeywords={wifi, code},
  pdfcreator={TeX},
  hidelinks,
]{hyperref}

% Font style
\RequirePackage[T1]{fontenc}
\renewcommand{\familydefault}{\sfdefault}
\RequirePackage[nopatch=footnote]{microtype}

% Font size
\RequirePackage{pgf}
\pgfmathsetmacro{\wifi@sizebaseline}{\wifi@size*1.2}
\renewcommand{\normalsize}{\fontsize{\wifi@size}{\wifi@sizebaseline}\selectfont}
\RequirePackage{anyfontsize}

% Change page dimensions
\RequirePackage[a4paper,landscape,margin=0pt]{geometry}

% Landscape
\RequirePackage{pdflscape}
\pagenumbering{gobble}

% QR codes
\RequirePackage{qrcode}
\newcommand{\qrc}[2]{%
  \ifwifi@border
  \fbox{%
    \fi
    \qrcode[height=\wifi@qrcode]{WIFI:T:WPA;P:#1;S:#2;H:false;}%
    \ifwifi@border%
  }
  \fi
}

% Spaces for password visibility every 4 char
\RequirePackage{xstring,pgffor}
\newcommand{\insertspaces}[1]{
  \StrLen{#1}[\length]%
  \foreach \i in {1,5,...,\length} {%
    \StrMid{#1}{\i}{\numexpr\i+3}
    \ifnum\i<\length\relax\,\fi
  }%
}

\AtBeginDocument{\centering\vspace*{\fill}}

\AtEndDocument{\vspace*{\fill}}
